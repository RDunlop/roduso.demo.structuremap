﻿namespace Roduso.Demo.Structuremap
{
    public interface ISecondQuery
    {
        void Execute();
        IDbContext DbContext { get; }
    }
}