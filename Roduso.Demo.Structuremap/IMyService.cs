﻿namespace Roduso.Demo.Structuremap
{
    public interface IMyService
    {
        IFirstQuery FirstQuery { get; }
        ISecondQuery SecondQuery { get; }
    }
}