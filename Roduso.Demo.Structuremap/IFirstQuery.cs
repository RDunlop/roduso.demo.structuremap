﻿namespace Roduso.Demo.Structuremap
{
    public interface IFirstQuery
    {
        void Execute();
        IDbContext DbContext { get; }
    }
}