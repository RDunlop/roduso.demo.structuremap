﻿namespace Roduso.Demo.Structuremap
{
    public class MyService : IMyService
    {
        public IFirstQuery FirstQuery { get; }
        public ISecondQuery SecondQuery { get; }

        public MyService(IFirstQuery firstQuery, ISecondQuery secondQuery)
        {
            FirstQuery = firstQuery;
            SecondQuery = secondQuery;
        }
    }
}