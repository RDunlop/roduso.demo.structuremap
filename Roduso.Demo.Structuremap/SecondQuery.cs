﻿namespace Roduso.Demo.Structuremap
{
    public class SecondQuery : ISecondQuery
    {
        public IDbContext DbContext { get; }

        public SecondQuery(IDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public void Execute()
        {
        }
    }
}