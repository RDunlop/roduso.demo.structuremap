﻿using NUnit.Framework;
using Shouldly;
using StructureMap;

namespace Roduso.Demo.Structuremap.Specs
{
    [TestFixture]
    public class WhenTheContainerIsCreated
    {
        [Test]
        public void FetchingATypeShouldResultInCorrespondingInstance()
        {
            var container = CreateContainer();

            container.GetInstance<IMyService>().ShouldBeOfType<MyService>();
        }

        [Test]
        public void FetchingTheSameTypeTwiceShouldResultInDifferentInstances()
        {
            var container = CreateContainer();

            container.GetInstance<IMyService>().ShouldNotBeSameAs(container.GetInstance<IMyService>());
        }

        [Test]
        public void TheSameTypeInAGraphShouldBeTheSameInstance()
        {
            var container = CreateContainer();

            var service = container.GetInstance<IMyService>();
            service.FirstQuery.DbContext.ShouldBeSameAs(service.SecondQuery.DbContext);
        }

        private static Container CreateContainer()
        {
            var container = new Container(c =>
            {
                c.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
            });
            return container;
        }
    }
}