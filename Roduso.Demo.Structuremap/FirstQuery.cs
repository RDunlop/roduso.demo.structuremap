﻿namespace Roduso.Demo.Structuremap
{
    public class FirstQuery : IFirstQuery
    {
        public IDbContext DbContext { get; }

        public FirstQuery(IDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public void Execute()
        {            
        }
    }
}