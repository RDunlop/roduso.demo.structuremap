﻿namespace Roduso.Demo.Structuremap
{
    public interface IDbContext
    {
        void Dispose();
    }
}